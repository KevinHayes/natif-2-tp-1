package ca.cegepdrummond.kevinhayes;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

/**
 * @author Kevin Hayes
 * @version %I%, %G%
 *
 * Cette classe représente la page principal que tu vois en ouvrant l'application.
 */
public class MenuPrincipal extends AppCompatActivity {

    /**
     * Crée l'interface pour le menu principal
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_principal);
    }

    /**
     * Crée une nouvelle note
     */
    public void newNote(View vue) {
        Intent noteIntent;
        noteIntent = new Intent(this, Note.class);
        startActivity(noteIntent);
    }
}



