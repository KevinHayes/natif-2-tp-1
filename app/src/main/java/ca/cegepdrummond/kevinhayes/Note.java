package ca.cegepdrummond.kevinhayes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

/**
 * @author Kevin Hayes
 * @version %I%, %G%
 *
 * Cette classe est la note ou tu peux ecrire
 */
public class Note extends AppCompatActivity {

    EditText entreeTexte;
    String texteEffacer;
    EditText titreNote;

    /**
     * Crée l'interface pour la note
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note);
        entreeTexte = findViewById(R.id.la_Note);
        titreNote = findViewById(R.id.Titre);
    }

    /**
     * Enlève ce qui a été écrit
     */
    public void undo(View vue) {
        if (!String.valueOf(entreeTexte.getText()).equals("")) {
            texteEffacer = String.valueOf(entreeTexte.getText());
            entreeTexte.setText("");
        }
    }

    /**
     * Remet ce qui a été undo
     */
    public void redo(View vue) {
        entreeTexte.setText(texteEffacer);
    }

    /**
     * retourne au menu principal
     */
    public void backToMenu(View view) {
        Intent menuIntent;
        menuIntent = new Intent(this, MenuPrincipal.class);
        startActivity(menuIntent);
    }
}